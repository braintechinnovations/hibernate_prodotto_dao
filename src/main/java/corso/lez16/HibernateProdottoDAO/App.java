package corso.lez16.HibernateProdottoDAO;

import java.util.List;

import corso.lez16.HibernateProdottoDAO.models.Prodotto;
import corso.lez16.HibernateProdottoDAO.models.crud.ProdottoDAO;

/**
 * Hello world!
 *
 */
public class App 
{
    public static void main( String[] args )
    {

    	ProdottoDAO proDao = new ProdottoDAO();
    	
    	//INSERT
    	
//    	Prodotto kinderBrioss = new Prodotto("KB123456", "Kinder Brioss", 2.69f);
//		Prodotto kinderFAA = new Prodotto("KB123457", "Kinder Fetta al Latte", 2.69f);
//		Prodotto KinderDelice = new Prodotto("KB123458", "Kinder Delice", 2.69f);
//		
//		proDao.insert(KinderDelice);
//		proDao.insert(kinderFAA);
//		proDao.insert(kinderBrioss);
    	
    	//FIND BY ID
//    	Prodotto prodUno = proDao.findById(2);
//    	System.out.println(prodUno);
    	
    	//REMOVE
//    	if(proDao.delete(1)) {
//    		System.out.println("Tutto ok!");
//    	} else {
//    		System.out.println("Errore ;(");
//    	}
    	
    	
    	//UPDATE
    	
//    	Prodotto prodUno = proDao.findById(2);
//    	prodUno.setNome("Kinder Querela");
//    	prodUno.setCodice("KFQUE");
//    	
//    	if(proDao.update(prodUno)) {
//    		System.out.println("Tutto ok!");
//    	} else {
//    		System.out.println("Errore ;(");
//    	}
    	
    	List<Prodotto> elencoProdotti = proDao.findAll();
    	
    	for(int i=0; i<elencoProdotti.size(); i++) {
    		Prodotto temp = elencoProdotti.get(i);
    		System.out.println(temp);
    	}
		

    }
}

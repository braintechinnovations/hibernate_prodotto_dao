package corso.lez16.HibernateProdottoDAO.models.crud;

import java.util.List;

import org.hibernate.Session;

import corso.lez16.HibernateProdottoDAO.models.Prodotto;
import corso.lez16.HibernateProdottoDAO.models.db.GestoreSessioni;

public class ProdottoDAO implements Dao<Prodotto>{

	@Override
	public void insert(Prodotto t) {

		Session sessione = GestoreSessioni.getIstanza().getFactory().getCurrentSession();
		
		try {
			sessione.beginTransaction();
			sessione.save(t);
			sessione.getTransaction().commit();
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			sessione.close();
		}
		
	}

	@Override
	public Prodotto findById(int id) {
		
		Session sessione = GestoreSessioni.getIstanza().getFactory().getCurrentSession();

		try {
			sessione.beginTransaction();
			
			Prodotto temp = sessione.get(Prodotto.class, id);
			
			sessione.getTransaction().commit(); 

			return temp;
		} catch (Exception e) {
			System.out.println(e.getMessage());
		} finally {
			sessione.close();
		}
		
		return null;
	}

	@Override
	public List<Prodotto> findAll() {

		Session sessione = GestoreSessioni.getIstanza().getFactory().getCurrentSession();

		try {
			sessione.beginTransaction();
			
			List<Prodotto> elenco = sessione.createQuery("FROM Prodotto").list();
			
			sessione.getTransaction().commit(); 
			
			return elenco;
			
		} catch (Exception e) {
			System.out.println(e.getMessage());
			
			return null;
		} finally {
			sessione.close();
		}
	}

	@Override
	public boolean delete(int id) {

		Session sessione = GestoreSessioni.getIstanza().getFactory().getCurrentSession();

		try {
			sessione.beginTransaction();
			
			//.get(...)				Cerca l'oggetto, se non esiste restituisce NULL
			//.load(...)			Cerca l'oggetto, se non esiste va in EXCEPTION

			Prodotto temp = sessione.load(Prodotto.class, id);
			sessione.delete(temp);
			
			sessione.getTransaction().commit(); 
			
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		} finally {
			sessione.close();
		}
	}

	@Override
	public boolean delete(Prodotto t) {
		
		Session sessione = GestoreSessioni.getIstanza().getFactory().getCurrentSession();

		try {
			sessione.beginTransaction();
			
			sessione.delete(t);
			
			sessione.getTransaction().commit(); 
			
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		} finally {
			sessione.close();
		}
	}

	@Override
	public boolean update(Prodotto t) {
		Session sessione = GestoreSessioni.getIstanza().getFactory().getCurrentSession();

		try {
			sessione.beginTransaction();
			
//			int id = t.getId();											//id del prodotto inserito nel metodo
//			Prodotto temp = sessione.load(Prodotto.class, id);			//Prodotto sul DB che devo modificare
//			
//			temp.setNome(t.getNome());
//			temp.setCodice(t.getCodice());
//			temp.setPrezzo(t.getPrezzo());
//			
//			sessione.update(temp);
			
			sessione.update(t);
			
			sessione.getTransaction().commit(); 
			
			return true;
		} catch (Exception e) {
			System.out.println(e.getMessage());
			return false;
		} finally {
			sessione.close();
		}
	}

}
